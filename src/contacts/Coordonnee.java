package contacts;

import java.util.ArrayList;

public class Coordonnee 
{
	private ArrayList<Mail> mails;
	private ArrayList<Telephone> telephones;
	
	public Coordonnee()
	{
		this.mails=new ArrayList<Mail>();
		this.telephones=new ArrayList<Telephone>();
	}
	
	public ArrayList<Mail> getMails() {
		return mails;
	}
	public void setMails(ArrayList<Mail> mails) {
		this.mails = mails;
	}
	public ArrayList<Telephone> getTelephones() {
		return telephones;
	}
	public void setTelephones(ArrayList<Telephone> telephones) {
		this.telephones = telephones;
	}
	
	public void addMail(Mail aMail)
	{
		this.mails.add(aMail);
	}
	public void removeMail(Mail aMail)
	{
		this.mails.remove(aMail);
	}
	public void addTelephone(Telephone aTelephone)
	{
		this.telephones.add(aTelephone);
	}
	public void removeTelephone(Telephone aTelephone)
	{
		this.telephones.remove(aTelephone);
	}
}
