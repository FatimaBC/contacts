package contacts;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.Box;
import javax.swing.JMenuBar;
import javax.swing.JLayeredPane;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JMenuItem;
import java.awt.Insets;

public class frame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame frame = new frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 912, 659);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(new Color(204, 204, 255));
		menuBar.add(Box.createHorizontalGlue());

		setJMenuBar(menuBar);
		
		JMenuItem mntmChercherUnContact = new JMenuItem("Chercher un contact");
		mntmChercherUnContact.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmChercherUnContact.setBackground(new Color(204, 204, 255));
		menuBar.add(mntmChercherUnContact);
		
		JMenuItem mntmVoirMesContacts = new JMenuItem("Voir mes contacts");
		mntmVoirMesContacts.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmVoirMesContacts.setBackground(new Color(204, 204, 255));
		menuBar.add(mntmVoirMesContacts);
		
		JMenuItem mntmSupprimerUnContact = new JMenuItem("Supprimer un contact");
		mntmSupprimerUnContact.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmSupprimerUnContact.setBackground(new Color(204, 204, 255));
		menuBar.add(mntmSupprimerUnContact);
		
		JMenuItem mntmAjouterUnContact = new JMenuItem("Ajouter un contact");
		mntmAjouterUnContact.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmAjouterUnContact.setBackground(new Color(204, 204, 255));
		menuBar.add(mntmAjouterUnContact);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setBackground(new Color(255, 255, 255));
		contentPane.add(panel, BorderLayout.CENTER);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
