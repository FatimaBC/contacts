package contacts;

import java.util.ArrayList;

public class Repertoire 
{
	private ArrayList<Contact> contacts;
	
	public Repertoire()
	{
		this.contacts=new ArrayList<Contact>();
	}
	
	public ArrayList<Contact> getContacts() {
		return contacts;
	}
	public void setContacts(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public void addContact(Contact aContact)
	{
		this.contacts.add(aContact);
	}
	public void removeContact(Contact aContact)
	{
		this.contacts.remove(aContact);
	}
	
}
