package contacts;

public class Contact 
{

	public enum Sex 
	{
		H,F;
	};
	private Sex sex;
	private String lastname;
	private String firstname;
	
	public Contact()
	{
		this.lastname="Unknown";
		this.firstname="Unknown";
		this.setSex(Sex.H);
	}
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}
	
	
}
