package contacts;

public class Telephone extends Coordonnee
{
	private String telephone;
	public enum Nature{
		Fixe, Mobile;
	};
	private Nature nature;
	
	public Telephone()
	{
		this.setTelephone("");
		this.setNature(null);
	}
	public Telephone(String aTel)
	{
		this.setTelephone(aTel);
		this.setNature(Nature.Mobile);
	}
	public Telephone(String aTel, Nature theNat)
	{
		this.setTelephone(aTel);
		this.setNature(theNat);
	}

	public Nature getNature() {
		return nature;
	}
	public void setNature(Nature nature) {
		this.nature = nature;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
